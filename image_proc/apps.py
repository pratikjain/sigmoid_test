from django.apps import AppConfig


class ImageProcConfig(AppConfig):
    name = 'image_proc'
