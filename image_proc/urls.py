from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('identify_image/', views.identify_image, name='identify_image'),
    path('success/', views.success, name='success'),
]