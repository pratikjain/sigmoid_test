from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from imageai.Detection import ObjectDetection
from django.template import loader
from django import forms
from django.core.files.storage import FileSystemStorage
from .models import *
import os


fs = FileSystemStorage(location = 'media/files/')

# Create your views here.
def index(request):
    return HttpResponse("Image Processing App!!")

class ImageForm(forms.ModelForm): 
  
    class Meta: 
        model = Image 
        fields = ['image'] 

def identify_image(request):
    if request.method == 'GET':
        form = ImageForm()
    elif request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
        return redirect('success')
    return render(request, 'image_proc/identify_image.html', {'form': form})

def success(request): 
    execution_path = os.getcwd()
    detector = ObjectDetection()
    detector.setModelTypeAsRetinaNet()
    detector.setModelPath( os.path.join(execution_path , "resnet50_coco_best_v2.0.1.h5"))
    detector.loadModel()
    detections = detector.detectObjectsFromImage(input_image=os.path.join(execution_path , "media/image.jpg"))
    string = ""
    for eachObject in detections:
        string += eachObject["name"] + "\n"
    return HttpResponse('successfuly uploaded!! Objects detected are: ' + string) 