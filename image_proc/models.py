from django.db import models
import os
from simple_proj import settings

def file_name(instance, filename):
    imgname = 'image.jpg'
    fullname = os.path.join(settings.MEDIA_ROOT, imgname)
    if os.path.exists(fullname):
        os.remove(fullname)
    return imgname
# Create your models here.
class Image(models.Model): 
    image = models.ImageField(upload_to=file_name) 